{-# LANGUAGE GADTs #-}

module PBT.Expr where

import Data.Text (Text)
import Test.Falsify

data Id a where
  F :: Text -> Id (a -> b)
  V :: Text -> Id a

data Binder a where
  Wild :: Id a -> Binder a

data Expr a where
  Var :: Id a -> Expr a
  Lit :: a -> Expr a
  App :: Expr (a -> b) -> Expr a -> Expr b
  Lam :: Binder a -> Expr b -> Expr (a -> b)

