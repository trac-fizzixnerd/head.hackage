{
  description = "head.hackage";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.ghc-artefact-nix.url = "github:mpickering/ghc-artefact-nix";
  inputs.ghc-artefact-nix.flake = false;
  inputs.all-cabal-hashes.url = "github:commercialhaskell/all-cabal-hashes/hackage";
  inputs.all-cabal-hashes.flake = false;
  inputs.hackage-security.url = "github:haskell/hackage-security";
  inputs.hackage-security.flake = false;
  inputs.overlay-tool.url = "github:bgamari/hackage-overlay-repo-tool";
  inputs.overlay-tool.flake = false;
  inputs.flake-compat.url = "github:edolstra/flake-compat";
  inputs.flake-compat.flake = false;
  outputs = sources@{nixpkgs, ...}:
  let
    env_x86 = import ./ci/default.nix { inherit sources; nixpkgs = nixpkgs.legacyPackages."x86_64-linux"; };
    env_aarch64 = import ./ci/default.nix { inherit sources; nixpkgs = nixpkgs.legacyPackages."aarch64-linux"; };
  in {
    devShells."x86_64-linux".default = env_x86;
    devShells."aarch64-linux".default = env_aarch64;
    hydraJobs.env."x86_64-linux" = env_x86;
    hydraJobs.env."aarch64-linux" = env_aarch64;
  };
}
