{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TestPatches
  ( testPatches
  , Config(..), config
  ) where

import Control.Monad
import Data.Foldable
import Data.List (intercalate)
import Data.Maybe
import Data.Text (Text)
import GHC.Generics
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Encoding.Error as TE
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BSL
import Data.Aeson
import qualified Data.Map.Strict as M
import qualified Data.Map.Merge.Strict as M
import qualified Data.Set as S

import qualified Distribution.Package as Cabal
import Distribution.Text
import Distribution.Types.Version hiding (showVersion)

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Text.PrettyPrint.ANSI.Leijen (Doc, vcat, (<+>))
import System.FilePath
import System.Directory
import System.Environment (getEnvironment)
import System.Exit
import System.Process.Typed
import System.IO.Temp
import System.IO
import Cabal.Plan
import NeatInterpolation
import Options.Applicative

import Types
import qualified MakeConstraints
import Utils

newtype BrokenPackages = BrokenPackages { getBrokenPackageNames :: S.Set PkgName }
  deriving (Semigroup, Monoid)

failureExpected :: BrokenPackages -> PkgName -> Bool
failureExpected (BrokenPackages pkgs) name = name `S.member` pkgs

-- Packages that should be built by declaring a @build-tool-depends@
-- dependency, not a @build-depends@ dependency. This is necessary for packages
-- that do not have a library component (e.g., @alex@).
newtype BuildToolPackages = BuildToolPackages { getBuildToolPackageNames :: S.Set PkgName }
  deriving (Semigroup, Monoid)

buildToolPackage :: BuildToolPackages -> PkgName -> Bool
buildToolPackage (BuildToolPackages pkgs) name = name `S.member` pkgs

data Config = Config { configPatchDir :: FilePath
                     , configCompiler :: FilePath
                     , configLoggingWrapper :: Maybe FilePath
                     , configGhcOptions :: [String]
                     , configCabalOptions :: [String]
                     , configOnlyPackages :: Maybe (S.Set Cabal.PackageName)
                     , configConcurrency :: Int
                     , configExtraCabalFragments :: [FilePath]
                     , configExtraPackages :: [(Cabal.PackageName, Version)]
                     , configTestPackages  :: [(Cabal.PackageName, FilePath)]
                     , configExpectedBrokenPkgs :: BrokenPackages
                     , configBuildToolPkgs :: BuildToolPackages
                     }

cabalOptions :: Config -> [String]
cabalOptions cfg =
  let
    compilerOption =
      maybe
        [ "-w", configCompiler cfg ]
        (\l -> [ "-w", l, "--with-hc-pkg", configCompiler cfg <> "-pkg" ])
        (configLoggingWrapper cfg)
  in
  configCabalOptions cfg ++
  compilerOption

config :: Parser TestPatches.Config
config =
  TestPatches.Config
    <$> patchDir
    <*> compiler
    <*> loggingWrapper
    <*> ghcOptions
    <*> cabalOptions
    <*> onlyPackages
    <*> concurrency
    <*> extraCabalFragments
    <*> extraPackages
    <*> testPackages
    <*> expectedBrokenPkgs
    <*> buildToolPkgs
  where
    patchDir = option str (short 'p' <> long "patches" <> help "patch directory" <> value "./patches")
    compiler = option str (short 'w' <> long "with-compiler" <> help "path of compiler")
    loggingWrapper =
      fmap Just (option str (long "logging-wrapper" <> help "path of compiler logging wrapper"))
      <|> pure Nothing
    ghcOptions = many $ option str (short 'f' <> long "ghc-option" <> help "flag to pass to compiler")
    cabalOptions = many $ option str (short 'F' <> long "cabal-option" <> help "flag to pass to cabal-install")
    onlyPackages =
      fmap (Just . S.fromList) (some $ option pkgName (short 'o' <> long "only" <> help "filter packages"))
      <|> pure Nothing
    concurrency = option auto (short 'j' <> long "concurrency" <> value 1 <> help "number of concurrent builds")
    extraCabalFragments = many $ option str (long "extra-cabal-fragment" <> help "path of extra configuration to include in cabal project files")
    extraPackages = many $ option pkgVer (short 'P' <> long "extra-package" <> help "other, un-patched packages to test")
    testPackages = many $ option pkgNamePath (short 'T' <> long "test-package" <> help "A package to run tests for")
    expectedBrokenPkgs =
      fmap (BrokenPackages . S.fromList) $ many
      $ option
          (fmap toPkgName pkgName)
          (short 'b' <> long "expect-broken" <> metavar "PKGNAME" <> help "expect the given package to fail to build")
    buildToolPkgs =
      fmap (BuildToolPackages . S.fromList) $ many
      $ option
          (fmap toPkgName pkgName)
          (long "build-tool-package" <> metavar "PKGNAME" <> help "declare the given package with build-tool-depends, not build-depends")

    pkgVer :: ReadM (Cabal.PackageName, Version)
    pkgVer = str >>= parse . T.pack
      where
        parse s
          | [name, ver] <- T.splitOn "==" s
          , Just ver' <- simpleParse $ T.unpack ver
          = pure (Cabal.mkPackageName $ T.unpack name, ver')
          | otherwise
          = fail $ unlines
              [ "Invalid extra package specified:"
              , "expected to be in form of PKG_NAME==VERSION"
              ]


    pkgNamePath :: ReadM (Cabal.PackageName, FilePath)
    pkgNamePath = str >>= parse . T.pack
      where
        parse s
          | [name, fp] <- T.splitOn "=" s
          = pure (Cabal.mkPackageName $ T.unpack name, T.unpack fp)
          | otherwise
          = fail $ unlines
              [ "Invalid test package specified:"
              , "expected to be in form of PKG_NAME=FILEPATH"
              ]

    pkgName :: ReadM Cabal.PackageName
    pkgName = str >>= maybe (fail "invalid package name") pure . simpleParse

testPatches :: Config -> IO ()
testPatches cfg = do
  setup cfg
  compInfo <- getCompilerInfo cfg
  packages <- findPatchedPackages (configPatchDir cfg)
  packages <- return (packages ++ configExtraPackages cfg)
  let packages' :: S.Set (Cabal.PackageName, Version)
      packages'
        | Just only <- configOnlyPackages cfg
        = S.fromList $ filter (\(pname,_) -> pname `S.member` only) packages
        | otherwise
        = S.fromList packages

  let build :: (Cabal.PackageName, Version) -> IO [TestedPatch LogOutput]
      build (pname, ver) = do
        res <- buildPackage cfg pname ver
        let tpatch = TestedPatch { patchedPackageName = PkgName $ T.pack $ display pname
                                 , patchedPackageVersion = Ver $ versionNumbers ver
                                 , patchedPackageResult = res
                                 }
        return [tpatch]

  testedPatches <- fold <$> mapConcurrentlyN (fromIntegral $ configConcurrency cfg) build (S.toList packages')

  let test :: (Cabal.PackageName, FilePath) -> IO ([TestedPatch LogOutput])
      test (pname, fpath) = do
        res <- testPackage cfg (pname, fpath)
        let tpatch = TestedPatch { patchedPackageName = PkgName $ T.pack $ display pname
                                 , patchedPackageVersion = Ver $ []
                                 , patchedPackageResult = res
                                 }
        return [tpatch]

  testResults <- fold <$> mapM test (configTestPackages cfg)

  let runResult = RunResult { testedPatches = testedPatches
                            , testedTests = testResults
                            , compilerInfo = compInfo
                            }

  let (okay, msg) = resultSummary (configExpectedBrokenPkgs cfg) runResult
  print msg
  BSL.writeFile "results.json" . encode =<< writeLogs "logs" runResult
  unless okay $ exitWith $ ExitFailure 1

writeLogs :: FilePath -> RunResult LogOutput -> IO (RunResult ())
writeLogs logDir runResult = do
    createDirectoryIfMissing True logDir
    let failedUnits = [ (unitId, log)
                      | (unitId, (buildInfo, result)) <- M.toList $ runResultUnits runResult
                      , Just log <- pure $
                          case result of
                            BuildSucceeded log -> Just log
                            BuildFailed log -> Just log
                            _ -> Nothing
                      ]
    mapM_ writeLog failedUnits
    return (() <$ runResult)
  where
    writeLog (UnitId unitId, LogOutput log) = TIO.writeFile logPath log
      where logPath = logDir </> T.unpack unitId

failedUnits :: BrokenPackages -> RunResult log
            -> M.Map UnitId (BuildInfo, BuildResult log)
failedUnits broken = M.filter didFail . runResultUnits
  where
    didFail (buildInfo, result) =
      case result of
        BuildFailed _ -> not $ failureExpected broken (pkgName buildInfo)
        _             -> False

planningErrors :: RunResult log -> [(PkgName, Ver)]
planningErrors runResult =
  [ (patchedPackageName tpatch, patchedPackageVersion tpatch)
  | tpatch <- testedPatches runResult ++ testedTests runResult
  , PackagePlanningFailed _ <- pure $ patchedPackageResult tpatch
  ]

resultSummary :: forall log. BrokenPackages -> RunResult log -> (Bool, Doc)
resultSummary broken runResult = (ok, msg)

  where
    ok = null planningErrs
          && null failedTests
          && null failedTestsBuild
          && null failedUnits
    msg = vcat
       [ "Total packages built:" <+> pshow (length allUnits)
       , ""
       , pshow (length planningErrs) <+> "had no valid install plan:"
       , PP.indent 4 $ vcat $ map (uncurry prettyPkgVer) planningErrs
       , ""
       , pshow (length failedUnits) <+> "packages failed to build:"
       , PP.indent 4 $ vcat
         [ prettyPkgVer (pkgName binfo) (version binfo)
         | (binfo, _) <- M.elems failedUnits ]
       , pshow (length expectedFailedUnits) <+> "packages failed to build (expected):"
       , PP.indent 4 $ vcat
         [ prettyPkgVer (pkgName binfo) (version binfo)
         | (binfo, _) <- M.elems expectedFailedUnits ]
       , pshow (length failedTargetUnits) <+> "target packages failed to build:"
       , PP.indent 4 $ vcat
         [ prettyPkgVer pkg ver
         | (pkg, ver) <- failedTargetUnits ]
       , ""
       , pshow (length failedDependsUnits) <+> "packages failed to build due to unbuildable dependencies."
       , ""
       , pshow (length failedTestsBuild) <+> "testsuites failed build."
       , PP.indent 4 $ vcat
          [ prettyPkgName pkg_name | pkg_name <- failedTestsBuild ]
       , pshow (length failedTests) <+> "testsuites failed."
       , PP.indent 4 $ vcat
          [ prettyPkgName pkg_name | pkg_name <- failedTests ]
       ]
    allUnits = runResultUnits runResult
    planningErrs = planningErrors runResult

    failedTests = [ pkg_name | (TestedPatch pkg_name ver (PackageResult (PackageBuildSucceeded PackageTestsFailed) _)) <- testedTests runResult ]

    failedTestsBuild = [ pkg_name | (TestedPatch pkg_name ver (PackageResult PackageBuildFailed _)) <- testedTests runResult ]

    failedTargetUnits =
        [ (patchedPackageName tp, patchedPackageVersion tp)
        | tp <- testedPatches runResult
        , not $ isSuccessfulPackageResult (patchedPackageResult tp)
        ]

    failedUnits, expectedFailedUnits :: M.Map UnitId (BuildInfo, BuildResult log)
    (expectedFailedUnits, failedUnits) = M.partition splitExpected (M.filter failed allUnits)
      where failed (_, BuildFailed _) = True
            failed _ = False

            splitExpected (binfo, _) = failureExpected broken (pkgName binfo)

    failedDependsUnits :: M.Map UnitId (S.Set UnitId)
    failedDependsUnits = M.filter (not . S.null) (failedDeps allUnits)

toPkgName :: Cabal.PackageName -> PkgName
toPkgName = PkgName . T.pack . display

toVer :: Version -> Ver
toVer = Ver . versionNumbers

prettyPkgName :: PkgName -> Doc
prettyPkgName (PkgName pname) =
  PP.blue (PP.text $ T.unpack pname)

-- | For @cabal-plan@ types.
prettyPkgVer :: PkgName -> Ver -> Doc
prettyPkgVer pname (Ver ver) =
  prettyPkgName pname
  <+> PP.green (PP.text $ intercalate "." $ map show ver)

-- | For @Cabal@ types.
prettyPackageVersion :: Cabal.PackageName -> Version -> Doc
prettyPackageVersion pname version =
  prettyPkgVer (toPkgName pname) (toVer version)

buildPackage :: Config -> Cabal.PackageName -> Version -> IO (PackageResult LogOutput)
buildPackage cfg pname version = do
  logMsg $ "=> Building" <+> prettyPackageVersion pname version
  compilerId <- getCompilerId (configCompiler cfg)

  -- prepare the test package
  createDirectoryIfMissing True dirName
  copyFile "cabal.project" (dirName </> "cabal.project")
  appendFile (dirName </> "cabal.project") "packages: .\n"
  appendFile (dirName </> "cabal.project") $ "package *\n  ghc-options:" ++ unwords (configGhcOptions cfg)
  TIO.writeFile
    (dirName </> concat ["test-", display pname, ".cabal"])
    (makeTestCabalFile cfg pname version)

  -- run the build
  code <- runProcess $ setWorkingDir dirName
                     $ proc "cabal"
                     $ ["new-build"] ++ cabalOptions cfg
  whatHappened ("=> Build of" <+> prettyPackageVersion pname version) cfg pname dirName code Nothing
  where
    dirName = "test-" ++ display pname ++ "-" ++ display version

testPackage :: Config -> (Cabal.PackageName, FilePath) -> IO (PackageResult LogOutput)
testPackage cfg (pname, fpath) = do
  logMsg $ "=> Testing" <+> prettyPackageName pname

  -- prepare the test package
  createDirectoryIfMissing True dirName
  copyFile "cabal.project" (dirName </> "cabal.project")
  appendFile (dirName </> "cabal.project") ("packages: " ++ fpath ++ "\n")
  -- run the build
  code <- runProcess $ setWorkingDir dirName
                     $ proc "cabal"
                     $ ["new-build", Cabal.unPackageName pname, "--enable-tests"] ++ cabalOptions cfg
  case code of
    ExitSuccess -> do
      runCode <- runProcess $ setWorkingDir dirName
                     $ proc "cabal"
                     $ ["new-test", Cabal.unPackageName pname, "--enable-tests"] ++ cabalOptions cfg
      whatHappened ("=> Test of" <+>  prettyPackageName pname) cfg pname dirName code (Just runCode)
    _ ->
      whatHappened ("=> Test of" <+>  prettyPackageName pname) cfg pname dirName code Nothing
  where
    dirName = "test-" ++ display pname

whatHappened herald cfg pname dirName code runCode = do
  compilerId <- getCompilerId (configCompiler cfg)
  let planPath = dirName </> "dist-newstyle" </> "cache" </> "plan.json"
  planExists <- doesFileExist planPath
  case planExists of
    True -> do
      Just plan <- decode <$> BSL.readFile planPath :: IO (Maybe PlanJson)
      cabalDir <- getCabalDirectory
      let logDir = cabalDir </> "logs" </> compilerId
      results <- mapM (checkUnit logDir) (pjUnits plan)
      logMsg $
        let result = case fromMaybe code runCode of
              ExitSuccess -> PP.cyan "succeeded"
              ExitFailure n -> PP.red "failed" <+> PP.parens ("code" <+> pshow n)
        in herald <+> result
      -- N.B. we remove the build directory on failure to ensure
      -- that we re-extract the source if the user re-runs after
      -- modifying a patch.
      unless (code == ExitSuccess) $ removeDirectoryRecursive dirName
      return $ PackageResult codesToStatus (mergeInfoPlan (planToBuildInfo plan) results)
    False -> do
      logMsg $ PP.red $ "=> Planning for" <+> herald <+> "failed"
      removeDirectoryRecursive dirName
      return $ PackagePlanningFailed mempty
  where
    codesToStatus =
      case code of
        ExitSuccess -> PackageBuildSucceeded $
                        case runCode of
                          Nothing -> NoTests
                          Just rCode -> case rCode of
                                           ExitSuccess -> PackageTestsSucceeded
                                           _ -> PackageTestsFailed
        _ -> PackageBuildFailed
    planToBuildInfo :: PlanJson -> M.Map UnitId BuildInfo
    planToBuildInfo plan = M.fromList
      [ (uId unit, info)
      | unit <- M.elems $ pjUnits plan
      , let depends :: S.Set UnitId
            depends = fold
              [ ciLibDeps comp <> ciExeDeps comp
              | comp <- M.elems $ uComps unit
              ]
      , let PkgId pname pvers = uPId unit
      , let info = BuildInfo { pkgName = pname
                             , version = pvers
                             , flags = uFlags unit
                             , dependencies = depends
                             }
      ]

    checkUnit :: FilePath -> Unit -> IO (BuildResult LogOutput)
    checkUnit logDir unit
      | UnitTypeBuiltin <- uType unit = return BuildPreexisted
      | UnitTypeLocal <- uType unit = return $ BuildSucceeded (LogOutput "<<inplace>>")
      | otherwise = do
      exists <- doesFileExist logPath
      case exists of
        True -> do
          buildLog <- TE.decodeUtf8With TE.lenientDecode <$> BS.readFile logPath
          let PkgId (PkgName unitPkgName) _pvers = uPId unit
          if | T.null buildLog
               -> return $ BuildFailed (LogOutput buildLog)
             | any isInstallingLine $ take 20 $ reverse $ T.lines buildLog
               -- Note that it's not enough to check for isInstallingLine, as
               -- it's possible for packages with custom Setup.hs scripts to
               -- fail even after installation has completed (e.g., Agda, as
               -- reported in #47). But only apply this check to the package
               -- being tested, as we only want to label the tested package as
               -- failing, not any of its dependencies.
             , not (Cabal.unPackageName pname == T.unpack unitPkgName) ||
               isPackageBuildSucceeded codesToStatus
               -> return $ BuildSucceeded (LogOutput buildLog)
             | otherwise
               -> return $ BuildFailed (LogOutput buildLog)
        False -> return BuildNotAttempted
      where
        isInstallingLine line = "Installing" `T.isPrefixOf` line
        logPath =
          case uId unit of
            UnitId uid -> logDir </> T.unpack uid <.> "log"

    mergeInfoPlan :: Ord k
                  => M.Map k BuildInfo
                  -> M.Map k (BuildResult log)
                  -> M.Map k (BuildInfo, BuildResult log)
    mergeInfoPlan = M.merge err err (M.zipWithMatched $ \_ x y -> (x,y))
      where
        err = M.mapMissing $ \_ _ -> error "error merging"



makeTestCabalFile :: Config -> Cabal.PackageName -> Version -> T.Text
makeTestCabalFile cfg pname' ver' =
  [text|
    cabal-version:       2.2
    name: test-$pname
    version: 1.0

    library
      exposed-modules:
      $depends
      default-language: Haskell2010
  |]
  where
    pname = T.pack $ display pname'
    ver = T.pack $ display ver'
    depends | buildToolPackage (configBuildToolPkgs cfg) (toPkgName pname')
            = "build-tool-depends: " <>
              pname <> ":" <> pname <> " == " <> ver
            | otherwise
            = "build-depends: " <> pname <> " == " <> ver

getCompilerInfo :: Config -> IO CompilerInfo
getCompilerInfo cfg = do
  (out,err) <- readProcess_ $ proc (configCompiler cfg) ["--info"]
  BSL.writeFile "compiler-info" out
  return $ CompilerInfo $ read $ T.unpack $ TE.decodeUtf8 $ BSL.toStrict out

setup :: Config -> IO ()
setup cfg = do
  keysExist <- doesDirectoryExist "keys"
  unless keysExist $ do
    cabalDir <- getCabalDirectory
    -- Work around cabal-install bug; it seems to get confused by repository changes
    removePathForcibly $ cabalDir </> "packages" </> repoName
    createDirectoryIfMissing True $ cabalDir </> "packages" </> repoName
    runProcess_ $ proc "build-repo.sh" ["gen-keys"]

  cwd <- getCurrentDirectory
  environ <- getEnvironment
  let env = environ ++
            [ ("REPO_NAME", repoName)
            , ("REPO_URL", "file://" ++ (cwd </> "repo"))
            , ("PATCHES", configPatchDir cfg)
            ]

  removePathForcibly "cabal.project"
  runProcess_
    $ setEnv env
    $ proc "build-repo.sh" ["build-repo"]

  projectFile <- openFile "cabal.project" WriteMode
  runProcess_
    $ setStdout (useHandleClose projectFile)
    $ setEnv env
    $ proc "build-repo.sh" ["build-repository-blurb"]

  extraFragments <- mapM readFile (configExtraCabalFragments cfg)
  constraints <- MakeConstraints.makeConstraints (configPatchDir cfg)
  appendFile "cabal.project" $ show $ vcat $
    [ "with-compiler: " <> PP.text (configCompiler cfg)
    , constraints
    ] ++ map PP.text extraFragments

  runProcess_ $ proc "cabal" ["new-update"]

  -- Force cabal to rebuild the index cache.
  buildPackage cfg "acme-box" (mkVersion [0,0,0,0])
  return ()
  where
    repoName = "local"

-- | Compute for each unit which of its dependencies failed to build.
failedDeps :: M.Map UnitId (BuildInfo, BuildResult log) -> M.Map UnitId (S.Set UnitId)
failedDeps pkgs =
  let res = fmap f pkgs -- N.B. Knot-tied

      f :: (BuildInfo, BuildResult log) -> S.Set UnitId
      f (binfo, result) =
        failedDirectDeps <> failedTransDeps
        where
          failedTransDeps = S.unions $ map (res M.!) (S.toList $ dependencies binfo)
          failedDirectDeps = S.filter failed $ S.filter excludeSelf (dependencies binfo)

          -- We don't want failures of units in the same package to count as
          -- failed dependencies.
          excludeSelf :: UnitId -> Bool
          excludeSelf unitId = pkgName binfo /= pkgName binfo'
            where (binfo', _) = pkgs M.! unitId

          failed :: UnitId -> Bool
          failed unitId =
            case snd $ pkgs M.! unitId of
              BuildFailed _ -> True
              _ -> False
  in res
