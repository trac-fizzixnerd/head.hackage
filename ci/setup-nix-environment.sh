# Bash snippet to be sourced in CI scripts.

cat >/etc/nix/nix.conf <<EOF
sandbox = false
build-users-group =
trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= loony-tools:pr9m4BkM/5/eSTZlkQyRt57Jz7OMBxNSUiMC4FkcNfk=
substituters = https://cache.nixos.org https://cache.zw3rk.com
experimental-features = nix-command flakes
cores = 1 # to avoid resource exhaustion. See: <https://gitlab.haskell.org/ghc/head.hackage/-/issues/38>
EOF

# Equivalent to running `nix develop` but works in CI scripts.
# We take care to avoid setting the temp dir as this will break unsandboxed builds.
# See: https://github.com/NixOS/nix/issues/1802
. <(nix print-dev-env | grep -v "export TE*MP" )
