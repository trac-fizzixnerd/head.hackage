{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}

module Types
  ( RunResult(..)
  , PackageStatus(..)
  , isPackageBuildSucceeded
  , PackageTestStatus(..)
  , runResultUnits
  , TestedPatch(..)
  , PackageResult(..)
  , isSuccessfulPackageResult
  , BuildInfo(..)
  , BuildResult(..)
  , LogOutput(..)
  , CompilerInfo(..)
  ) where

import Cabal.Plan
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.Aeson
import qualified Data.Text as T
import GHC.Generics
import Cabal.Plan

-- | Information about a unit which we attempted to build.
data BuildInfo
  = BuildInfo { pkgName :: PkgName
              , version :: Ver
              , flags :: M.Map FlagName Bool
              , dependencies :: S.Set UnitId
              }
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

-- | The result of a unit build.
data BuildResult log
  = BuildSucceeded { buildLog :: log }
    -- ^ the build succeeded.
  | BuildPreexisted
    -- ^ the unit pre-existed in the global package database.
  | BuildFailed { buildLog :: log }
    -- ^ the build failed
  | BuildNotAttempted
    -- ^ the build was not attempted either because a dependency failed or it
    -- is an executable or testsuite component
  deriving stock (Show, Generic, Functor, Foldable, Traversable)
  deriving anyclass (ToJSON, FromJSON)

data PackageTestStatus = NoTests | PackageTestsFailed | PackageTestsSucceeded
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)


data PackageStatus = PackageBuildFailed | PackageBuildSucceeded PackageTestStatus
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

isPackageBuildSucceeded :: PackageStatus -> Bool
isPackageBuildSucceeded PackageBuildSucceeded{} = True
isPackageBuildSucceeded PackageBuildFailed      = False

-- | The result of an attempt to tested a patch
data PackageResult log
  = PackagePlanningFailed { planningError :: T.Text }
    -- ^ Our attempt to build the package resulting in no viable install plan.
  | PackageResult { packageStatus :: PackageStatus
                  , units :: M.Map UnitId (BuildInfo, BuildResult log)
                  }
    -- ^ We attempted to build the package.
  deriving stock (Show, Generic, Functor, Foldable, Traversable)
  deriving anyclass (ToJSON, FromJSON)

isSuccessfulPackageResult :: PackageResult log -> Bool
isSuccessfulPackageResult PackagePlanningFailed{} = False
isSuccessfulPackageResult PackageResult{packageStatus} = isPackageBuildSucceeded packageStatus

-- | Information about a patch which we tested.
data TestedPatch log
  = TestedPatch { patchedPackageName :: PkgName
                , patchedPackageVersion :: Ver
                , patchedPackageResult :: PackageResult log
                }
  deriving stock (Show, Generic, Functor, Foldable, Traversable)
  deriving anyclass (ToJSON, FromJSON)

-- | The result of a CI run.
data RunResult log
  = RunResult { testedPatches :: [TestedPatch log]
              , testedTests   :: [TestedPatch log]
              , compilerInfo  :: CompilerInfo
              }
  deriving stock (Show, Generic, Functor, Foldable, Traversable)
  deriving anyclass (ToJSON, FromJSON)

runResultUnits :: RunResult log -> M.Map UnitId (BuildInfo, BuildResult log)
runResultUnits runResult = M.unions
  [ units
  | tpatch <- testedPatches runResult ++ testedTests runResult
  , PackageResult _ units <- pure $ patchedPackageResult tpatch
  ]

-- | Logged output from a build.
newtype LogOutput = LogOutput { getLogOutput :: T.Text }
  deriving stock (Eq, Ord, Show)
  deriving newtype (ToJSON, FromJSON)

newtype CompilerInfo = CompilerInfo [(String, String)]
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)
