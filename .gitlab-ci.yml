# GHC/head.hackage CI support
# ===========================
#
# This is the GitLab CI automation that drives GHC's head.hackage testing.
# The goal is to be able to test GHC by building a subset of Hackage.
# Moreover, we want to be able to collect logs of failed builds as well as
# performance metrics from builds that succeed.
#
# To accomplish this we use the ci executable in ./ci. This drives a set of
# cabal v2-build builds and preserves their results.
#
# The execution flow looks something like:
#
# - Gitlab runner
#   - (nix run)
#     - run-ci
#       - ./run-ci (the Nix package just wraps the script)
#         - (nix run) (when USE_NIX=1)
#           - head-hackage-ci $EXTRA_OPTS (a Cabal project in ci/)
#             - ci/Main.hs
#               - TestPatches.testPatches <$> TestPatches.config
#                 - option '--test-package'
#               - <something similar for building the packages>
#
# EXTRA_OPTS are injected into the execution flow inside ./run-ci, which in turn
# sources them from ci/config.sh.
#
# The compiler to be tested can be taken from a number of sources.
# head.hackage's own validation pipeline runs against GHC HEAD and the three
# supported major versions. In addition, other GitLab projects (e.g. ghc/ghc>)
# can trigger a multi-project pipeline, specifying a GHC binary distribution
# via either the GHC_TARBALL or UPSTREAM_* variables.
#

variables:
  # Which nixos/nix Docker image tag to use
  DOCKER_TAG: "2.13.1"

  # Default this to ghc/ghc> to make it more convenient to run from the web
  # interface.
  UPSTREAM_PROJECT_ID: 1
  UPSTREAM_PROJECT_PATH: "ghc/ghc"

  GIT_SUBMODULE_STRATEGY: recursive

  # CPUS is set by the runner, as usual.

  # EXTRA_HC_OPTS are passed to via --ghc-options to GHC during the package
  # builds. This is instantiated with, e.g., -dcore-lint during GHC validation
  # builds.

  # ONLY_PACKAGES can be passed to restrict set of packages that are built.

  # EXTRA_OPTS are passed directly to test-patches.

  # Multi-project pipeline variables:
  #
  # These are set by the "upstream" pipeline for downstream pipelines:
  #
  #   UPSTREAM_PROJECT_PATH: The path of the upstream project (e.g. `ghc/ghc`)
  #   UPSTREAM_PIPELINE_ID: The ID of the upstream pipeline
  #
  # Instead of UPSTREAM_PIPELINE_ID you can also pass:

  #   UPSTREAM_COMMIT_SHA: The ref or commit SHA of the GHC build to be tested
  #

  # We explictly set the locale to avoid happy choking up on UTF-8 source code. See #31
  LANG: "C.UTF-8"

stages:
  - generate
  - dispatch


generate-pipeline:
  variables:
    GIT_SUBMODULE_STRATEGY: none
  image: alpine:latest
  tags: [x86_64-linux]
  stage: generate
  script: ./ci/generate-pipeline.sh
  artifacts:
    paths:
      - gitlab-generated-pipeline.yml

run-pipeline:
  stage: dispatch
  trigger:
    strategy: depend
    forward:
      pipeline_variables: true
    include:
      - artifact: gitlab-generated-pipeline.yml
        job: generate-pipeline
